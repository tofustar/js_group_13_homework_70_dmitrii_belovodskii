import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { SendFormService } from '../shared/send-form.service';
import { Form } from '../shared/form.model';

@Component({
  selector: 'app-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.css']
})
export class MainFormComponent implements OnInit {
  @ViewChild('f') userForm!: NgForm;

  constructor(private sendFormService: SendFormService) { }

  ngOnInit(): void {
  }

  onSubmit() {

    const form = new Form(
      this.userForm.value.name,
      this.userForm.value.surname,
      this.userForm.value.patronymic,
      this.userForm.value.phone,
      this.userForm.value.job,
      this.userForm.value.selectType,
      this.userForm.value.size,
      this.userForm.value.comment
    );

    this.sendFormService.addForm(form).subscribe();

  }
}
